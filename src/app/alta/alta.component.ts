import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.scss']
})
export class AltaComponent implements OnInit {

  @Output() outputInfo: EventEmitter<any> = new EventEmitter<any>();
  nombre: string ;
  edad: number;
  constructor() { }

  ngOnInit() {
  }

  ingresar(){
    this.outputInfo.emit(
      {
        nombre: this.nombre,
        edad: this.edad
      }
    );
  }

}
