import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.scss']
})
export class Compo1Component implements OnInit, OnChanges {

  @Input() pruebaInput: string = "Default";
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(evento){
    console.log(evento);
  }
}
