import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private router: Router) { }
  numero1: number;
  numero2: number;
  operacion;
  resultado;
  nombreDesdeAlta;
  edadDesdeAlta;
  title = 'test';
  valor: string = "Default";
  modificarInput() {
    this.valor = "CHAU";
  }


  redireccionUsuarios() {
    this.router.navigate(['usuario']);
  }
  redireccionAlta(){
    this.router.navigate(['alta']);
  }

  calcular(){
    console.log("NUMERO 1: ", this.numero1);
    console.log("NUMERO 2: ", this.numero2);
    console.log("Operacion: ", this.operacion);

    switch(this.operacion){
      case '+':
        this.resultado = this.numero1 + this.numero2;
        break;
      case '-':
        this.resultado = this.numero1 - this.numero2;
        break;
      case '/':
        this.resultado = this.numero1 / this.numero2;
        break;
      case '*':
        this.resultado = this.numero1 * this.numero2;
        break;
    }
  }

  capturarInfo(event){
    this.nombreDesdeAlta = event.nombre;
    this.edadDesdeAlta = event.edad;
    console.log(event);
  }

}
