import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario/usuario.component';
import { AltaComponent } from './alta/alta.component';


const routes: Routes = [
  {
    path: 'usuario',
    component: UsuarioComponent
  },
  {
    path: 'alta',
    component: AltaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
