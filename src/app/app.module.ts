import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Compo1Component } from './compo1/compo1.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AltaComponent } from './alta/alta.component';

@NgModule({
  declarations: [
    AppComponent,
    Compo1Component,
    UsuarioComponent,
    AltaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
